class Piece(object):

	def __init__(self, piece_data):
		self.is_straight = True if 'length' in piece_data else False
		self.radius = piece_data.get('radius')
		self.angle  = piece_data.get('angle')
		self.length = piece_data.get('length')
		self.is_switch = piece_data.get('switch', False)

		if not self.length:
			self.length = (2 * 3.14159 * self.radius * self.angle) / 360.

	def turn_direction(self):
		if not self.angle:
			return 0
		elif self.angle < 0:
			return -1
		else:
			return 1
