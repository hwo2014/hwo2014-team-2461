import json
import socket
import sys
import time

from track import Track
from piece import Piece
from lane import Lane
from car import Car
from car_position import CarPosition
from lap import Lap
from turbo import Turbo

class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.cur_throttle = 0
        self.cur_piece_index = -1
        self.turbo = None
        self.is_on_turbo = False
        self.color = None
        self.race_has_begun = False
        self.want_to_switch = 0
        self.base_throttle_level = 0.85
        self.slowing_into_curve = False
        self.slowing_into_curve_begun = False
        self.slowing_into_curve_from_piece_index = None
        self.turbo_straightaway_threshold = 800
        self.prev_slip_angle = 0
        self.slip_angle_warn_level = 28
        self.slip_angle_critical_level = 39
        self.slip_angle_delta_warn_level = 3.0


    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def on_your_car(self, data):
        self.color = data['color']
        self.ping()

    def set_throttle(self, throttle):
        print("Throttle to %s" % throttle)
        self.msg("throttle", throttle)

    def switch_lanes(self, direction):
        if direction.lower() == 'left':
            self.msg("switchLane", "Left")
        elif direction.lower() == 'right':
            self.msg("switchLane", "Right")
        else:
            raise ValueError("Input to switch_lanes must be either 'Left' or 'Right'")

    def engage_turbo(self):
        if self.turbo:
            self.msg('turbo', 'ENGAGE BEAST MODE')
            self.turbo = None

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_init(self, data):
        if not self.color:
            print("Did not yet receive color... exiting")
            sys.exit("No color received from server")
        self.track = Track(data['race']['track'])
        self.total_laps = data['race']['raceSession']['laps']
        self.max_lap_time = data['race']['raceSession']['maxLapTimeMs']
        self.quick_race = data['race']['raceSession']['quickRace']
        self.cars = {c['id']['color']: Car(c) for c in data['race']['cars']}

        print
        print("TRACK DATA:")
        print
        print("Pieces")
        print("------")
        for n, p in enumerate(self.track.pieces):
            if p.is_straight:
                print("\t-->Piece %s: length %s, %s, next turn: %s" % (n, p.length, "has switch" if p.is_switch else "has no switch", self.track.next_turn[n]))
            else:
                print ("\t-->Piece %s: radius %s, angle %s, next turn: %s" % (n, p.radius, p.angle, self.track.next_turn[n]))
        print
        print("Lanes")
        print("-----")
        for lane in self.track.lanes:
            print("\t-->Lane %s: distance from center %s" % (lane.index, lane.distance_from_center))
        print

    def on_game_start(self, data):
        print("Race has begun!")
        self.race_has_begun = True
        self.set_throttle(self.base_throttle_level)
        self.cur_throttle = self.base_throttle_level
        self.ping()

    def on_car_positions(self, data, game_tick):
        command_sent = False

        for c in data:
            self.cars[c['id']['color']].pos = CarPosition(c)

        # If we're crashed, just quit now
        if self.cars[self.color].is_dead:
            #print("Our car is dead...")
            self.ping()
            return

        # Check if we're approaching a curve
        if not self.slowing_into_curve and self._in_straight_piece(self.cars[self.color]) and self.track.calc_straightaway_dist(self.cars[self.color].pos) < (250 if not self.is_on_turbo else 560):
            self.slowing_into_curve = True
            self.slowing_into_curve_from_piece_index = self.cars[self.color].pos.piece_index
            desired_throttle = max(0.5, self.cur_throttle * 0.7)
            if self.is_on_turbo:
                desired_throttle = min(0.2, desired_throttle)
            print("Approaching a curve, decelerating.")
            self.set_throttle(desired_throttle)
            self.cur_throttle = desired_throttle
            command_sent = True
        elif self.slowing_into_curve and not self._in_straight_piece(self.cars[self.color]):
            #print("Turning into the curve...")
            self.slowing_into_curve_begun = True
        elif self.slowing_into_curve and self.slowing_into_curve_begun and self._in_straight_piece(self.cars[self.color]):# and self.cars[self.color].pos.piece_index != self.slowing_into_curve_from_piece_index:
            print("Leaving curve, accelerating.")
            self.slowing_into_curve = False
            self.slowing_into_curve_begun = False
            self.slowing_into_curve_from_piece_index = None
            self.set_throttle(self.base_throttle_level)
            self.cur_throttle = self.base_throttle_level
            command_sent = True

        #if command_sent:
        #    print("Sent a command regarding curves...")

        # Check our slip angle
        our_slip_angle = self.cars[self.color].pos.angle
        slip_delta = abs(our_slip_angle) - abs(self.prev_slip_angle)
        if slip_delta > self.slip_angle_delta_warn_level and not command_sent:
            print("Delta of %s (%s -> %s) exceeds warn level of %s! Slowing down extra hard." % (
                slip_delta, self.prev_slip_angle, our_slip_angle, self.slip_angle_delta_warn_level)
            )
            desired_throttle = max(0.1, self.cur_throttle * 0.2)
            self.set_throttle(desired_throttle)
            command_sent = True
            self.cur_throttle = desired_throttle
        elif abs(our_slip_angle) > self.slip_angle_warn_level and not command_sent:
            print("Warning: slip angle is now %s (warn level: %s)" % (our_slip_angle, self.slip_angle_warn_level))
            if abs(our_slip_angle) > self.slip_angle_critical_level:
                print("\t--> CRITICAL! Slowing down really hard.")
                desired_throttle = max(0.15, self.cur_throttle * 0.2)
            else:
                desired_throttle = max(0.5, self.cur_throttle * 0.85)
            self.set_throttle(desired_throttle)
            command_sent = True
            self.cur_throttle = desired_throttle
        else:
            if self.race_has_begun and not command_sent and self.cur_throttle < self.base_throttle_level and (not self.slowing_into_curve or (abs(self.cars[self.color].pos.angle) < 20 and self._is_at_apex(self.cars[self.color]) and self.track.pieces[(self.cars[self.color].pos.piece_index + 1) % len(self.track.pieces)].is_straight)):
                print("Reaccelerating...")
                desired_throttle = max(0.2, min(self.base_throttle_level, self.cur_throttle * 1.35))
                self.set_throttle(desired_throttle)
                command_sent = True
                self.cur_throttle = desired_throttle
        self.prev_slip_angle = our_slip_angle

        #if command_sent:
        #    print("Sent a command regarding slip angles...")

        # If we have a straightaway and turbo is available, hit the turbo!
        if self.turbo and not command_sent:
            straightaway_dist = self.track.calc_straightaway_dist(self.cars[self.color].pos)
            if straightaway_dist > self.turbo_straightaway_threshold:
                print("Engaging BEAST MODE! (straightaway distance: %s, threshold: %s)" % (
                    straightaway_dist, self.turbo_straightaway_threshold)
                )
                self.engage_turbo()
                command_sent = True

        if command_sent:
            print("Sent a command regarding turbo...")

        # Check to see if we've entered a new track piece
        #print("Checking if we're in new piece...")
        
        if self.cars[self.color].pos.piece_index != self.cur_piece_index and game_tick > 0:
            #print("We are!")
            self.on_piece_enter(data)
            self.cur_piece_index = self.cars[self.color].pos.piece_index
            
        if not command_sent and self.want_to_switch:
            if self.want_to_switch == -1:
                #print("Switching left...")
                self.switch_lanes('Left')
            else:
                #print("Switching right...")
                self.switch_lanes('Right')
            self.want_to_switch = 0
            command_sent = True
        
        #print("Leaving on_car_positions")
        self.ping()

    def on_piece_enter(self, data):
        #print("Entering on_piece_enter...")
        next_turn = self.track.next_turn[self.cars[self.color].pos.piece_index]
        if next_turn < 0 and self.cars[self.color].pos.end_lane_index > 0:
            self.want_to_switch = -1
        elif next_turn > 0 and self.cars[self.color].pos.end_lane_index < len(self.track.lanes) - 1:
            self.want_to_switch = 1
        #print("Exiting on_piece_enter...")

    def on_turbo_available(self, data):
        self.turbo = Turbo(data)
        print("Turbo available (duration = %s, factor = %s)" %
            (self.turbo.duration_millis / 1000., self.turbo.factor)
        )
        self.ping()

    def on_turbo_start(self, data):
        self.is_on_turbo = True
        self.set_throttle(max(self.cur_throttle * 0.7, 0.5))

    def on_turbo_end(self, data):
        self.is_on_turbo = False

    def on_lap_finished(self, data):
        lap_obj = Lap(data)
        print("%s in the %s car completed lap %s (lap time: %s, total time: %s, laps to go: %s)" %
            (data['car']['name'], data['car']['color'], lap_obj.lap_number, lap_obj.lap_millis / 1000.,
             lap_obj.total_millis / 1000., self.total_laps - lap_obj.lap_number - 1)
        )
        self.cars[data['car']['color']].laps.append(lap_obj)

    def on_crash(self, data):
        print
        print('**********************************')
        print("*** %s CRASHED (%s)" % (data['name'].upper(), data['color'].upper()))
        print('**********************************')
        print
        if data['color'] == self.color:
            self.cur_throttle = 0.0
        self.cars[data['color']].is_dead = True
        self.ping()

    def on_spawn(self, data):
        print("%s respawned (%s)" % (data['name'], data['color']))
        if data['color'] == self.color:
            self.set_throttle(self.base_throttle_level)
            self.cur_throttle = self.base_throttle_level
            self.prev_slip_angle = 0
        self.cars[data['color']].is_dead = False
        self.ping()

    def on_finish(self, data):
        print("Player name %s finished in the %s car" % (data['name'], data['color']))
        car = self.cars[data['color']]
        for lap in car.laps:
            print("\t--> Lap %s: %s seconds (%s), in position %s" %
                (lap.lap_number, lap.lap_millis / 1000., lap.total_millis / 1000.,
                 lap.ranking))
        print("\t--> Total time: %s, in position %s" %
            (car.laps[-1].total_millis / 1000., car.laps[-1].ranking))
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_dnf(self, data):
        print("Did not finish the race")
        print(data)
        self.ping()

    def on_tournament_end(self, data):
        print("Tournament ended")

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'yourCar': self.on_your_car,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'turboAvailable': self.on_turbo_available,
            'turboStart': self.on_turbo_start,
            'turboEnd': self.on_turbo_end,
            'lapFinished': self.on_lap_finished,
            'crash': self.on_crash,
            'spawn': self.on_spawn,
            'finish': self.on_finish,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'dnf': self.on_dnf,
            'tournamentEnd': self.on_tournament_end,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                if msg_type == 'carPositions':
                    #print msg
                    msg_map[msg_type](data, msg.get('gameTick', 0))
                else:
                    msg_map[msg_type](data)
            else:
                print("Got {0} (no handler available)".format(msg_type))
                self.ping()
            line = socket_file.readline()

    #############
    # INTERNALS #
    #############

    def _in_straight_piece(self, car):
        return self.track.pieces[car.pos.piece_index].is_straight

    def _get_actual_radius(self, car, piece):
        lane = self.track.lanes[car.pos.end_lane_index]
        if piece.turn_direction() < 0:
            return piece.radius + lane.distance_from_center
        else:
            return piece.radius - lane.distance_from_center

    def _get_bend_length(self, radius, angle):
        return (2 * 3.14159 * radius * angle) / 360.

    def _is_at_apex(self, car):
        p = self.track.pieces
        if self.track.pieces[car.pos.piece_index].is_straight:
            return False
        else:
            cur_piece = self.track.pieces[car.pos.piece_index]
            actual_radius = self._get_actual_radius(car, cur_piece)
            return car.pos.in_piece_dist >= self._get_bend_length(actual_radius, cur_piece.angle) / 1.75

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
