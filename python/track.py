from piece import Piece
from lane import Lane

class Track(object):

	def __init__(self, track_data):
		self.id = track_data.get('id')
		self.name = track_data.get('name')
		self.pieces = [Piece(p) for p in track_data['pieces']]
		self.next_turn = [
			0 if not p.is_straight else self.get_next_turn_from_piece_index(n) for n, p in enumerate(self.pieces)]
		self.lanes = [Lane(L) for L in track_data['lanes']]

	def calc_straightaway_dist(self, car_pos):
		result = 0.0
		cur_piece_index = car_pos.piece_index
		starting_index = cur_piece_index
		cur_in_piece_pos = car_pos.in_piece_dist
		while self.pieces[cur_piece_index].is_straight:
			result += (self.pieces[cur_piece_index].length - cur_in_piece_pos)
			cur_in_piece_pos = 0.0
			cur_piece_index = (cur_piece_index + 1) % len(self.pieces)

			# If we've gone all the way around, break (shouldn't happen in a loop track...)
			if cur_piece_index == starting_index:
				break
		return result

	def get_next_turn_direction(self, car_pos):
		cur_piece_index = car_pos.piece_index
		starting_index = cur_piece_index
		while self.pieces[cur_piece_index].is_straight:
			cur_piece_index = (cur_piece_index + 1) % len(self.pieces)
			
			# If we've gone all the way around, break (shouldn't happen in a loop track...)
			if cur_piece_index == starting_index:
				return 0

		# -1 = left turn, 1 = right turn
		return -1 if self.pieces[cur_piece_index].angle < 0 else 1

	def get_next_turn_from_piece_index(self, piece_index):
		cur_piece_index = piece_index
		starting_index = cur_piece_index
		while self.pieces[cur_piece_index].is_straight:
			cur_piece_index = (cur_piece_index + 1) % len(self.pieces)

			# If we've gone all the way around, break (shouldn't happen in a loop track...)
			if cur_piece_index == starting_index:
				return 0

		# -1 = left turn, 1 = right turn
		return -1 if self.pieces[cur_piece_index].angle < 0 else 1
