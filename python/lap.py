class Lap(object):

	def __init__(self, lap_data):
		self.lap_number = lap_data['lapTime']['lap']
		self.lap_ticks  = lap_data['lapTime']['ticks']
		self.lap_millis = lap_data['lapTime']['millis']
		self.laps_completed = lap_data['raceTime']['laps']
		self.total_ticks = lap_data['raceTime']['ticks']
		self.total_millis = lap_data['raceTime']['millis']
		self.ranking = lap_data['ranking']['overall']
		self.fastest_lap = lap_data['ranking']['fastestLap']
