from lap import Lap

class Car(object):

    def __init__(self, car_data):
        self.name = car_data['id']['name']
        self.color = car_data['id']['color']
        self.length = car_data['dimensions']['length']
        self.width = car_data['dimensions']['width']
        self.guide_flag_pos = car_data['dimensions']['guideFlagPosition']
        self.is_dead = False
        self.laps = []
        self.pos = None
