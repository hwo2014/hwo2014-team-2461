class CarPosition(object):

	def __init__(self, pos_data):
		self.angle = pos_data['angle']
		self.piece_index = pos_data['piecePosition']['pieceIndex']
		self.in_piece_dist = pos_data['piecePosition']['inPieceDistance']
		self.start_lane_index = pos_data['piecePosition']['lane']['startLaneIndex']
		self.end_lane_index = pos_data['piecePosition']['lane']['endLaneIndex']
		self.is_switching_lanes = self.start_lane_index != self.end_lane_index
		self.lap = pos_data['piecePosition']['lap']
